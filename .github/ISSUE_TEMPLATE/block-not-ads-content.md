---
name: Block not ads content
about: Report when ABPVN List block usefull content in website
title: 'Block incorrect on website: [domain]'
labels: Inccorect block
assignees: ''

---

**Incorrect block description**
Tell about incorrect block

**Step to reproduce**
1. Go to website
2. ...
3. ...

**Website url**
Direct website url contains ads

<details>
<summary>With ABPVN Enabled</summary>
![Image name](imageUrl)
</details>
<details>
<summary>Without ABPVN Enabled</summary>
![image name](imageUrl)
</details>

**Environment**
|Browser|Browser Version|Extentions/Adblocker Programs|Adblocker Version|Subscription Filter|
|---------|----------------|--------------------------------|-----------------|-------------------|
|Ex: Firefox, Chrome|Ex: 91|Ex: Adblock Plus|Ex: 3.10|Ex: EasyList + ABPVN List, EasyPrivacy|
