---
name: Report ads on website
about: Report ads exist on website when enable ABPVN List
title: 'Exist ads on website: [domain]'
labels: Exist ads
assignees: ''

---

**Ads description**
Tell about ads you see in website here.

**Step to reproduce**
1. Go to website
2. ...
3. ...

**Website url**
Direct website url contains ads

**Screenshot**
<details>
<summary>With ABPVN Enabled</summary>
![Image name](imageUrl)
</details>
<details>
<summary>Without ABPVN Enabled</summary>
![image name](imageUrl)
</details>

**Environment**
|Browser|Browser Version|Extentions/Adblocker Programs|Adblocker Version|Subscription Filter|
|---------|----------------|--------------------------------|-----------------|-------------------|
|Ex: Firefox, Chrome|Ex: 91|Ex: Adblock Plus|Ex: 3.10|Ex: EasyList + ABPVN List, EasyPrivacy|
